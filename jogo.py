from PPlay.sound import *
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *

musica = Sound("musicas/pokemon.ogg")
musica.play()
janela = Window(898, 657)
janela.set_title("Jogaynho")
fundo = GameImage("imagens/battle.png")
ball = Sprite("imagens/pokeball.png", 4)

ball.set_position(251, 142)
ball.set_total_duration(400)
velocidadex = 1
velocidadey = 1

# fundo = GameImage("fundo.jpg")
while True:
    ball.move_x(velocidadex)
    ball.move_y(velocidadey)
    if (int(ball.x) == janela.width - 30) or (int(ball.x) == 0):
        velocidadex = velocidadex * -1
    if (int(ball.y) == janela.height - 30) or (int(ball.y) == 0):
        velocidadey = velocidadey * -1

    fundo.draw()
    ball.draw()
    ball.update()
    janela.update()
